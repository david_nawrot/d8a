<?php

namespace Drupal\accountancy_invoice\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\node\Entity\Node;

class InvoiceAjaxForm {
  public static function customerValues(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
    $response = new AjaxResponse();
    //$form['ajax_container']['field_work']['widget']['#access'] = TRUE;
    //$response->addCommand(new ReplaceCommand('#user-full-name', '<span class="unit">Whatever</span>'));
    $title = "anc";
    $content = "whatever";
    $options = array('width' => '80%', 'appendTo' => 'body');
    //$selector = '#user-full-name';
    $response->addCommand(new OpenModalDialogCommand($title, $content));
    //return $response;
    
    //$form['field_service_name']['widget']['0']['value']['#required'] = FALSE;

    //ksm($form['field_service_name']);

    $value = $form_state->getValue('field_invoice_customer');
    $target_id = $value[0]['target_id'];

    $entity_to_load = Node::load($target_id);

    //
    


    //ksm($entity_to_load->field_country->getString());
    
    //$response->addCommand(new HtmlCommand('#user-full-name', $entity_to_load->field_city->value));

    // Fill in company name
    $form['ajax_container']['customer_data_fieldset']['field_invoice_company']['widget']['0']['value']['#value'] = $entity_to_load->field_customer_s_invoice_name->value;
    // Fill in address
    $form['ajax_container']['customer_data_fieldset']['field_invoice_address']['widget']['0']['value']['#value'] = $entity_to_load->field_address->value;
    // Fill in postal code
    $form['ajax_container']['customer_data_fieldset']['field_invoice_postal_code']['widget']['0']['value']['#value'] = $entity_to_load->field_postal_code->value;
    // Fill in city
    $form['ajax_container']['customer_data_fieldset']['field_invoice_city']['widget']['0']['value']['#value'] = $entity_to_load->field_city->value;

    // Fill in country
    // Fill in country - this is specific, because in this case we need to retirieve a list of allowed values. 
    // By default loading an entity will return just a single key, not the value itself, so we need to 
    // load a list of allowed values that are appended to this field. It's been done down below: 
    $entityManager = \Drupal::service('entity_field.manager');
    $fields = $entityManager->getFieldStorageDefinitions('node', 'customer');
    $options = options_allowed_values($fields['field_country']);
    // Now, having list of options we can access the value by calling array of options and providing key.
    $form['ajax_container']['customer_data_fieldset']['field_invoice_country']['widget']['0']['value']['#value'] = $options[$entity_to_load->field_country->value];

    // Fill in NIP
    $form['ajax_container']['customer_data_fieldset']['field_invoice_nip']['widget']['0']['value']['#value'] = $entity_to_load->field_nip->value;

    // Fill in e-mail
    $form['ajax_container']['customer_data_fieldset']['field_invoice_e_mail']['widget']['0']['value']['#value'] = $entity_to_load->field_e_mail->value;



    // Handle with work field
    $work = $form_state->getValue('field_work');
    $options = $form['ajax_container']['field_work']['widget']['#options'];

    // Select only this type of work which is assigned to selected customer
    // $query = db_select('node__field_customer', 'customer')
    //            ->condition('customer.field_customer_target_id', $target_id, '=')
    //            ->fields('customer', array('entity_id'));
    // $result = $query->execute();

    
    //$narrowed_options = array();

    
    // Find only this type of work which is not invoiced yet
    // $invoiced = db_select('node__field_invoiced', 'invoiced')
    //             ->condition('invoiced.field_invoiced_value', 1, '=')
    //             ->fields('invoiced', array('entity_id'));
    // $invoiced = $invoiced->execute()->fetchAllKeyed();

    // Options are stored in two places in a form
    // One is just #options and second one are keys right in the form next to other element
    // so we need to change both.

    // Create narrowed options array excuding work that is already invoiced
    //foreach ($result as $record) {


//      if (array_key_exists($record->entity_id, $invoiced)) {

        
        // Load node, so we can get price and currency and present it rather then stick with default title label
  //      $work_to_load = Node::load($record->entity_id);
  //      $work_price = $work_to_load->field_price->value;
  //      $work_currency = $work_to_load->field_currency->value;

        // Modify label of work, so it includes price and currency
   //     $narrowed_options[$record->entity_id] = $options[$record->entity_id] . ' (' . $work_price . ' ' . $work_currency . ')';

        // Modify label of work, so it includes price and currency
    //    $form['ajax_container']['field_work']['widget'][$record->entity_id]['#title'] = $options[$record->entity_id] . ' (' . $work_price . ' ' . $work_currency . ')';
    //  }
   // }

    $narrowed_options = self::getFilteredWork($target_id, $options);
    foreach($narrowed_options as $key => $value) {
      $form['ajax_container']['field_work']['widget'][$key]['#title'] = $value;
    }
    //ksm($my_options);
    //ksm($narrowed_options);

    // Assign narrowed options to form options
    $form['ajax_container']['field_work']['widget']['#options'] = $narrowed_options;

    //ksm($form['ajax_container']['field_work']);
    // Unset undesired options from form
    foreach($options as $key => $value) {
      if (!array_key_exists($key, $narrowed_options)) {
        //unset($form['ajax_container']['field_work']['widget']['work']['table_work'][$key]);
        unset($form['ajax_container']['field_work']['widget'][$key]);
      }
    }

    return $form['ajax_container'];
  }

  public static function price(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $value = $form_state->getValue('field_work');
    //ksm($value);
    $price = 0;
    if (!empty($value)) {
      foreach ($value as $key => $value) {
        if (is_array($value)) {
          $entity_to_load = Node::load($value['target_id']);
          $work_price = $entity_to_load->field_price->value;
          //$work_currency = $entity_to_load->field_currency->value;
          $price += $work_price;
        }
      }
    }
    $form['field_invoice_price']['widget']['0']['value']['#value'] = $price;
    return $form['field_invoice_price'];
  }

  public static function getFilteredWork($customer_id, $options, $exceptions = NULL) {
    $filtered_options = array();

    // Select only this type of work which is assigned to selected customer
    $query = db_select('node__field_customer', 'customer')
               ->condition('customer.field_customer_target_id', $customer_id, '=')
               ->fields('customer', array('entity_id'));
    $query = $query->execute();

    // Find only this type of work which is not invoiced yet or is invoiced but applies to currently 
    // edited node.
    $invoiced = db_select('node__field_invoiced', 'invoiced')->fields('invoiced', array('entity_id'));
    $db_or = db_or();
    $db_or->condition('invoiced.field_invoiced_value', 1, '=');
    if (!empty($exceptions)) {
      foreach($exceptions as $key => $value) {
        $db_or->condition('invoiced.entity_id', $value, '=');    
      }
    }
    $invoiced->condition($db_or);
    $invoiced = $invoiced->execute()->fetchAllAssoc('entity_id');
    
    foreach ($query as $record) {
      if (array_key_exists($record->entity_id, $invoiced)) {
        $work_to_load = Node::load($record->entity_id);
        $work_price = $work_to_load->field_price->value;
        $work_currency = $work_to_load->field_currency->value;
        $filtered_options[$record->entity_id] = $options[$record->entity_id] . ' (' . $work_price . ' ' . $work_currency . ')';
      }
    }

    return $filtered_options;
  }
}

