<?php

namespace Drupal\accountancy_invoice\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\EntityOwnerInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsButtonsWidget;
use Drupal\node\Entity\Node;

/**
 * Plugin implementation of the 'field reference' widget.
 *
 * @FieldWidget(
 *   id = "field_reference",
 *   module = "accountancy_invoice",
 *   label = @Translation("Custom reference widget"),
 *   field_types = {
 *     "entity_reference"
 *   },
 *   multiple_values = TRUE
 * )
 */

class LatestReferenceWidget extends OptionsButtonsWidget {
  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    //ksm($element);

    $users = array(
      array('uid' => 1, 'first_name' => 'Indy', 'last_name' => 'Jones'),
      array('uid' => 2, 'first_name' => 'Darth', 'last_name' => 'Vader'),
      array('uid' => 3, 'first_name' => 'Super', 'last_name' => 'Man'),
    );

    $header = array(
      'title' => t('Title'),
      'price' => t('Price'),
      'image' => t('Image'),
    );

    // Initialize an empty array
    $options = array();
    // Next, loop through the $users array
    foreach ($users as $user) {
      // each element of the array is keyed with the UID
      $options[$user['uid']] = array(
        // 'first_name' was the key used in the header
        'first_name' => $user['first_name'],
        // 'last_Name' was the key used in the header 
        'last_name' => $user['last_name'], 
      );
    }

    //ksm($options);
    $build_table = array();
    foreach($element['#options'] as $key => $value) {
      $entity_to_load = Node::load($key);
      $build_table[$key] = array(
        'title' =>  $entity_to_load->title->value,
        'price' => $entity_to_load->field_price->value,
        'image' => 'anc', //$entity_to_load->field_project_preview,
      );
    }

    
    //ksm($entity_to_load);

    $work['work'] = array(
      '#type' => 'fieldset',
      '#title' => t('Work'),
    );

    $work['work']['table_work'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $build_table,
      //'#default_value' => array(0, 1, 1),
      '#js_select' => FALSE,
    );

    //ksm($build_table);

    return $work;
    
  }
}
