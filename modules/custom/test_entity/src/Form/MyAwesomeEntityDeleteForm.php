<?php

namespace Drupal\test_entity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting My awesome entity entities.
 *
 * @ingroup test_entity
 */
class MyAwesomeEntityDeleteForm extends ContentEntityDeleteForm {


}
