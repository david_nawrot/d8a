<?php

namespace Drupal\test_entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for My awesome entity edit forms.
 *
 * @ingroup test_entity
 */
class MyAwesomeEntityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\test_entity\Entity\MyAwesomeEntity */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label My awesome entity.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label My awesome entity.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.my_awesome_entity.canonical', ['my_awesome_entity' => $entity->id()]);
  }

}
