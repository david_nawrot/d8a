<?php

namespace Drupal\test_entity\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the My awesome entity entity.
 *
 * @ingroup test_entity
 *
 * @ContentEntityType(
 *   id = "my_awesome_entity",
 *   label = @Translation("My awesome entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\test_entity\MyAwesomeEntityListBuilder",
 *     "views_data" = "Drupal\test_entity\Entity\MyAwesomeEntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\test_entity\Form\MyAwesomeEntityForm",
 *       "add" = "Drupal\test_entity\Form\MyAwesomeEntityForm",
 *       "edit" = "Drupal\test_entity\Form\MyAwesomeEntityForm",
 *       "delete" = "Drupal\test_entity\Form\MyAwesomeEntityDeleteForm",
 *     },
 *     "access" = "Drupal\test_entity\MyAwesomeEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\test_entity\MyAwesomeEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "my_awesome_entity",
 *   admin_permission = "administer my awesome entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/my_awesome_entity/{my_awesome_entity}",
 *     "add-form" = "/admin/structure/my_awesome_entity/add",
 *     "edit-form" = "/admin/structure/my_awesome_entity/{my_awesome_entity}/edit",
 *     "delete-form" = "/admin/structure/my_awesome_entity/{my_awesome_entity}/delete",
 *     "collection" = "/admin/structure/my_awesome_entity",
 *   },
 *   field_ui_base_route = "my_awesome_entity.settings"
 * )
 */
class MyAwesomeEntity extends ContentEntityBase implements MyAwesomeEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  public function setTestExchangeRate($currency, $time) {
    if (date('Y-m-d', $time) == date('Y-m-d', time())) {
      $yesterday = date('Y-m-d', ($time-86400));
      $day_number = date('N', ($time-86400));
      // Exclude from Sunday
      if ($day_number == '7') {
        $yesterday = date('Y-m-d', ($time-(86400*3)));
      // Exclude from Saturday
      } elseif ($day_number == '6') {
        $yesterday = date('Y-m-d', ($time-(86400*2)));
      }
    } else {
      $yesterday = date('Y-m-d', $time);
      $day_number = date('N', $time);
      if ($day_number == '7') {
        // Exclude from Sunday
        $yesterday = date('Y-m-d', ($time-(86400*2)));
      // Exclude from Saturday
      } elseif ($day_number == '6') {
        $yesterday = date('Y-m-d', ($time-(86400)));
      }
    }
    
    $uri = 'http://api.nbp.pl/api/exchangerates/rates/a/'.$currency.'/'.$yesterday.'/?format=json';

    try {
      $response = \Drupal::httpClient()->get($uri, array('headers' => array('Accept' => 'text/plain')));
      $data = (string) $response->getBody();
      if (empty($data)) {
        return FALSE;
      } else {
        $data = json_decode($data, TRUE);
        $exchange = $data['rates']['0']['mid'];
      }
    }
    catch (RequestException $e) {
      return FALSE;
    }
    
    $this->set('test_exchange_rate', $exchange);
    return $this;
  }

  public static function getCurrencyExchange($currency, $time) {
    if (date('Y-m-d', $time) == date('Y-m-d', time())) {
      // Yesterday
      $day_number = date('N', ($time-86400));

      // Exclude from Sunday
      if ($day_number == '7') {
        $date_to_pass = date('Y-m-d', ($time-(86400*3)));
      // Exclude from Saturday
      } elseif ($day_number == '6') {
        $date_to_pass = date('Y-m-d', ($time-(86400*2)));
      } else {
        $date_to_pass = date('Y-m-d', ($time-86400));
      }

      $yesterday_midnight = strtotime($date_to_pass.' 00:00:00');
      $yesterday_second_before_midnight = strtotime($date_to_pass.' 23:59:59');

      $storage = \Drupal::entityQuery('my_awesome_entity')
        ->condition('test_currency', $currency, '=')
        ->condition('test_date', $yesterday_midnight, '>=')
        ->condition('test_date', $yesterday_second_before_midnight, '<=')
        ->execute();

      if (count($storage) < 1) {
        return FALSE;
      } else {
        return $storage;
      }
    } else {
      // Selcted day
      $day_number = date('N', $time);

      // Exclude from Sunday
      if ($day_number == '7') {
        $date_to_pass = date('Y-m-d', ($time-(86400*2)));
      // Exclude from Saturday
      } elseif ($day_number == '6') {
        $date_to_pass = date('Y-m-d', ($time-86400));
      } else {
        $date_to_pass = date('Y-m-d', $time);
      }

      $midnight = strtotime($date_to_pass . '00:00:00');
      $second_before_midnight = strtotime($date_to_pass . '23:59:59');

      $storage = \Drupal::entityQuery('my_awesome_entity')
        ->condition('test_currency', $currency, '=')
        ->condition('test_date', $midnight, '>=')
        ->condition('test_date', $second_before_midnight, '<=')
        ->execute();

      if (count($storage) < 1) {
        return FALSE;  
      } else {
        return $storage;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    
    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the My awesome entity entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
  
    $fields['test_currency'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Currency'))
      ->setDescription(t('Select currency'))
      ->setRequired(TRUE)
      ->setTranslatable(FALSE)
      ->setRevisionable(FALSE)
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'options_select',
        'weight' => -4,
      ))
      ->setSettings(array(
        'allowed_values' => array(
          'usd' => 'USD',
          'eur' => 'EUR',
          'gbp' => 'GBP',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);



    $fields['test_exchange_rate'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Exchange'))
      ->setDescription(t('exchange'))
      ->setRequired(TRUE)
      ->setSettings(['default_value', 0])
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'float',
        'weight' => -4,
      ))
      // This is responsible for displaying a field while creating new entity
      // This allows to display a form field on entity add form
      ->setDisplayOptions('form', array(
        'type' => 'number_float',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['test_date'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Date'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'datetime_timestamp',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the My awesome entity is published.'))
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  public function getTestCurrency() {
    return $this->get('test_currency')->value;
  }

  public function getTestExchangeRate() {
    return $this->get('test_exchange_rate')->value;
  }

  public function getTestDate() {
    return $this->get('test_date')->value;
  }
 
  /**
   * Returns 
   */
  public function getTestDateFormatted($format) {
    $date = $this->get('test_date')->value;
    $date = date($format, $date);
    return $date;
  }

  public function setTestDate($timestamp) {
    if (date('Y-m-d', $timestamp) == date('Y-m-d', time())) {
      // Yesterday
      $day_number = date('N', ($timestamp-86400));
      // Exclude from Sunday
      if ($day_number == '7') {
        $timestamp_to_pass = ($timestamp-(86400*3));
      // Exclude from Saturday
      } elseif ($day_number == '6') {
        $timestamp_to_pass = ($timestamp-(86400*2));
      } else {
        $timestamp_to_pass = ($timestamp-86400);
      }
      $this->set('test_date', $timestamp_to_pass);
    } else {
      // Selcted day
      $day_number = date('N', $timestamp);
      // Exclude from Sunday
      if ($day_number == '7') {
        $timestamp_to_pass = ($timestamp-(86400*2));
      // Exclude from Saturday
      } elseif ($day_number == '6') {
        $timestamp_to_pass = ($timestamp-86400);
      } else {
        $timestamp_to_pass = $timestamp;
      }
      $this->set('test_date', $timestamp_to_pass);
    }
    return $this;
  }
}
