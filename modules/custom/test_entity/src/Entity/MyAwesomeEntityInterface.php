<?php

namespace Drupal\test_entity\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining My awesome entity entities.
 *
 * @ingroup test_entity
 */
interface MyAwesomeEntityInterface extends  ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the My awesome entity name.
   *
   * @return string
   *   Name of the My awesome entity.
   */
  //public function getName();

  /**
   * Sets the My awesome entity name.
   *
   * @param string $name
   *   The My awesome entity name.
   *
   * @return \Drupal\test_entity\Entity\MyAwesomeEntityInterface
   *   The called My awesome entity entity.
   */
  //public function setName($name);

  /**
   * Gets the My awesome entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the My awesome entity.
   */
  public function getCreatedTime();

  /**
   * Sets the My awesome entity creation timestamp.
   *
   * @param int $timestamp
   *   The My awesome entity creation timestamp.
   *
   * @return \Drupal\test_entity\Entity\MyAwesomeEntityInterface
   *   The called My awesome entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the My awesome entity published status indicator.
   *
   * Unpublished My awesome entity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the My awesome entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a My awesome entity.
   *
   * @param bool $published
   *   TRUE to set this My awesome entity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\test_entity\Entity\MyAwesomeEntityInterface
   *   The called My awesome entity entity.
   */
  public function setPublished($published);

  public function getTestCurrency();
  public function getTestExchangeRate();
  public function getTestDate();
  public function setTestExchangeRate($currency, $time);
  public function setTestDate($time);
}
