<?php

namespace Drupal\test_entity\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for My awesome entity entities.
 */
class MyAwesomeEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
