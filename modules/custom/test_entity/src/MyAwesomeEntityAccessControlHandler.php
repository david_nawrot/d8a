<?php

namespace Drupal\test_entity;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the My awesome entity entity.
 *
 * @see \Drupal\test_entity\Entity\MyAwesomeEntity.
 */
class MyAwesomeEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\test_entity\Entity\MyAwesomeEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished my awesome entity entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published my awesome entity entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit my awesome entity entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete my awesome entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add my awesome entity entities');
  }

}
