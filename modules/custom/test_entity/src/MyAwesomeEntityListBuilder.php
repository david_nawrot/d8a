<?php

namespace Drupal\test_entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of My awesome entity entities.
 *
 * @ingroup test_entity
 */
class MyAwesomeEntityListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('My awesome entity ID');
    $header['test_exchange_rate'] = $this->t('Exchange rate');
    $header['test_currency'] = $this->t('Test currency');
    $header['test_date'] = $this->t('Date');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\test_entity\Entity\MyAwesomeEntity */
    $row['id'] = $entity->id();
    $row['test_exchange_rate'] = $entity->getTestExchangeRate();
    //$row['test_currency'] = $entity->getTestCurrency();
    
    $row['test_currency'] = $this->l(
      $entity->getTestCurrency(),
      new Url(
        'entity.my_awesome_entity.canonical', array(
          'my_awesome_entity' => $entity->id(),
        )
      )
    );

    $row['test_date'] = $entity->getTestDateFormatted('Y-m-d H:i:s');
    return $row + parent::buildRow($entity);
  }

}
