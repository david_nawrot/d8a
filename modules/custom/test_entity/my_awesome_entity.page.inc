<?php

/**
 * @file
 * Contains my_awesome_entity.page.inc.
 *
 * Page callback for My awesome entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for My awesome entity templates.
 *
 * Default template: my_awesome_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_my_awesome_entity(array &$variables) {
  // Fetch MyAwesomeEntity Entity Object.
  $my_awesome_entity = $variables['elements']['#my_awesome_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
