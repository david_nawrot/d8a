<?php

namespace Drupal\aaa;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Aaa entity.
 *
 * @see \Drupal\aaa\Entity\aaa.
 */
class aaaAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\aaa\Entity\aaaInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished aaa entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published aaa entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit aaa entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete aaa entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add aaa entities');
  }

}
