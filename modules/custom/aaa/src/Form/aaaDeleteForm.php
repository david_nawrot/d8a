<?php

namespace Drupal\aaa\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Aaa entities.
 *
 * @ingroup aaa
 */
class aaaDeleteForm extends ContentEntityDeleteForm {


}
