<?php

namespace Drupal\aaa\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Aaa entities.
 *
 * @ingroup aaa
 */
interface aaaInterface extends  ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Aaa name.
   *
   * @return string
   *   Name of the Aaa.
   */
  public function getName();

  /**
   * Sets the Aaa name.
   *
   * @param string $name
   *   The Aaa name.
   *
   * @return \Drupal\aaa\Entity\aaaInterface
   *   The called Aaa entity.
   */
  public function setName($name);

  /**
   * Gets the Aaa creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Aaa.
   */
  public function getCreatedTime();

  /**
   * Sets the Aaa creation timestamp.
   *
   * @param int $timestamp
   *   The Aaa creation timestamp.
   *
   * @return \Drupal\aaa\Entity\aaaInterface
   *   The called Aaa entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Aaa published status indicator.
   *
   * Unpublished Aaa are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Aaa is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Aaa.
   *
   * @param bool $published
   *   TRUE to set this Aaa to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\aaa\Entity\aaaInterface
   *   The called Aaa entity.
   */
  public function setPublished($published);

}
